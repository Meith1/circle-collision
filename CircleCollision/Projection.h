#ifndef __Projection__
#define __Projection__

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Projection
{
public:
	Projection();
	~Projection();

	glm::mat4 getProjectionMatrix();

private:
	glm::mat4 m_pProjectionMatrix;
};

#endif
