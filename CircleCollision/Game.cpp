#include "Game.h"
#include "Window.h"
#include "InputHandler.h"

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <algorithm>
#include <vector>

int secondSmallestIndex;

Game* Game::s_pInstance = 0;

const glm::vec2 windowSize = glm::vec2(1280, 960);

Game::Game()
{
	m_pShader = new GLSLShader();
}

Game::~Game()
{
	delete m_pShader;
	for (int i = 0; i < 2; i++)
	{
		delete m_pCubeContainer[i];
	}
}

bool Game::init()
{
	Window::Instance()->createWindow("Physics Engine", windowSize.x, windowSize.y, 0);

	//load shader
	m_pShader->loadFromFile(GL_VERTEX_SHADER, "shaders/shader.vert");
	m_pShader->loadFromFile(GL_FRAGMENT_SHADER, "shaders/shader.frag");

	//compile and link shader 
	m_pShader->createAndLinkProgram();
	m_pShader->use();
	{
		//add attributes and uniforms
		m_pShader->addAttribute("vVertex");
		m_pShader->addAttribute("vColor");
		m_pShader->addUniform("MVP");
	}
	m_pShader->unUse();

	m_bRunning = true;
	m_bShapeInit = false;
	checkOrResolve = false;
	return true;
}

void Game::handleEvents()
{
	static int count = 0;
	glm::vec3 veclocity = glm::vec3(1.0f);

	InputHandler::Instance()->update();

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_I) && count < 1)
	{
		count++;
		m_pCube = new Cube();
		m_pCube->setPosition(glm::vec3(-10.0f, 0.0f, 0.0f));
		m_pCube->setRotation(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->setScale(glm::vec3(5.0f, 5.0f, 5.0f));
		m_pCube->setVelocity(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->setAngularVelocity(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->setVertexColor(glm::vec3(1.0f, 0.0f, 0.0f));
		m_pCube->init(m_pShader);
		m_pCubeContainer.push_back(m_pCube);

		m_pCube = new Cube();
		m_pCube->setPosition(glm::vec3(10.0f, 0.0f, 0.0f));
		m_pCube->setRotation(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->setScale(glm::vec3(5.0f, 5.0f, 5.0f));
		m_pCube->setVelocity(glm::vec3(0.5f, 0.5f, 0.0f));
		m_pCube->setAngularVelocity(glm::vec3(0.1f, 0.1f, 0.1f));
		m_pCube->setVertexColor(glm::vec3(0.0f, 0.0f, 1.0f));
		m_pCube->init(m_pShader);
		m_pCubeContainer.push_back(m_pCube);

		m_pCube = new Cube();
		m_pCube->setPosition(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->setRotation(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->setScale(glm::vec3(40.0f, 40.0f, 40.0f));
		m_pCube->setVertexColor(glm::vec3(0.0f, 0.0f, 0.0f));
		m_pCube->init(m_pShader);
		m_pCubeContainer.push_back(m_pCube);

		m_pSat = new SAT(m_pCubeContainer[0], m_pCubeContainer[1]);

		m_bShapeInit = true;
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_W))
	{
		glm::vec3 displacement = Camera::Instance()->CamLookDirection() * veclocity;
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() + displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_S))
	{
		glm::vec3 displacement = Camera::Instance()->CamLookDirection() * veclocity;
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() - displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_D))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() + displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_A))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() - displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
	}
}

void Game::update()
{
	
	if (m_bShapeInit)
	{
		// detect collision
		if (!checkOrResolve)
		{
			m_pSat->init();
			checkOrResolve = m_pSat->update();
		}

		// resolve collision
		if (checkOrResolve)
		{
			m_pCubeContainer[0]->setOldVelocity(m_pCubeContainer[0]->getVelocity());
			m_pCubeContainer[1]->setOldVelocity(m_pCubeContainer[1]->getVelocity());

			m_pCubeContainer[0]->setOldAngularVelocity(m_pCubeContainer[0]->getAngularVelocity());
			m_pCubeContainer[1]->setOldAngularVelocity(m_pCubeContainer[1]->getAngularVelocity());

			m_pCubeContainer[0]->setVelocity(glm::vec3(0));
			m_pCubeContainer[1]->setVelocity(glm::vec3(0));

			m_pCubeContainer[0]->setAngularVelocity(glm::vec3(0));
			m_pCubeContainer[1]->setAngularVelocity(glm::vec3(0));

			m_pCubeContainer[1]->setPosition(m_pCubeContainer[1]->getPosition() - m_pSat->getMTV());

			getCubePoints(m_pCubeContainer[0], m_pCubeContainer[1]);

			int smallestIndexCube1 = closestPointIndex(m_pCubeContainer[1], P1);
			int secondSmallestIndexCube1 = secondSmallestIndex;
			int smallestIndexCube2 = closestPointIndex(m_pCubeContainer[0], P2);
			int secondSmallestIndexCube2 = secondSmallestIndex;

			glm::vec3 poc;
			if (m_pSat->getCollisionType() == 1)
			{
				poc = glm::distance(P1[smallestIndexCube1], m_pCubeContainer[1]->getPosition()) < glm::distance(P2[smallestIndexCube2], m_pCubeContainer[0]->getPosition()) ? P1[smallestIndexCube1] : P2[smallestIndexCube2];
			}
			else
			{
				glm::vec3 v1 = P1[smallestIndexCube1] - P1[secondSmallestIndexCube1];
				glm::vec3 v2 = P2[smallestIndexCube2] - P2[secondSmallestIndexCube2];

				poc.x = ((P1[smallestIndexCube1].x * v2.x) - (P2[smallestIndexCube2].x * v1.x)) / (v1.x - v2.x);
				poc.y = ((P1[smallestIndexCube1].y * v2.y) - (P2[smallestIndexCube2].y * v1.y)) / (v1.y - v2.y);
				poc.z = ((P1[smallestIndexCube1].z * v2.z) - (P2[smallestIndexCube2].z * v1.z)) / (v1.z - v2.z);

				if ((v1.x - v2.x) == 0)
				{
					if (v1.y != 0)
					{
						poc.x = ((poc.y - P1[smallestIndexCube1].y / v1.y) + v1.x);
					}
					else if (v1.z != 0)
					{
						poc.x = ((poc.z - P1[smallestIndexCube1].z / v1.z) + v1.x);
					}
				}

				if ((v1.y - v2.y) == 0)
				{
					if (v1.x != 0)
					{
						poc.y = ((poc.x - P1[smallestIndexCube1].x / v1.x) + v1.y);
					}
					else if (v1.z != 0)
					{
						poc.y = ((poc.z - P1[smallestIndexCube1].z / v1.z) + v1.y);
					}
				}

				if ((v1.z - v2.z) == 0)
				{
					if (v1.y != 0)
					{
						poc.z = ((poc.y - P1[smallestIndexCube1].y / v1.y) + v1.z);
					}
					else if (v1.x != 0)
					{
						poc.z = ((poc.x - P1[smallestIndexCube1].x / v1.x) + v1.z);
					}
				}
			}

			resolveCollision(m_pCubeContainer[0], m_pCubeContainer[1], poc);
			checkOrResolve = false;
		}

		//m_pCircle.update();
		for (int i = 0; i < m_pCubeContainer.size(); i++)
		{
			m_pCubeContainer[i]->update();
		}
		Camera::Instance()->update();
		wallCollision();
	}
}

void Game::render()
{
	glClearDepth(1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//glEnable(GL_CULL_FACE);

	//glCullFace(GL_BACK);

	if (m_bShapeInit)
	{
		//m_pCircle.render(m_pShader);
		for (int i = 0; i < m_pCubeContainer.size(); i++)
		{
			m_pCubeContainer[i]->render(m_pShader);
		}
	}

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	Window::Instance()->swapBuffers();
}

void Game::close()
{
	//m_pCircle.close();
	for (int i = 0; i < m_pCubeContainer.size(); i++)
	{
		m_pCubeContainer[i]->close();
		delete m_pCubeContainer[i];
	}
	SDL_DestroyWindow(Window::Instance()->getwindow());
}

void Game::quit()
{
	m_bRunning = false;
	SDL_Quit();
}

void Game::wallCollision()
{
	for (int i = 0; i < m_pCubeContainer.size() - 1; i++)
	{
		if ((m_pCubeContainer[i]->getPosition().x + (m_pCubeContainer[i]->getRadius() * m_pCubeContainer[i]->getScale().x)) > (m_pCubeContainer[m_pCubeContainer.size() - 1]->getPosition().x + (m_pCubeContainer[m_pCubeContainer.size() - 1]->getRadius() * m_pCubeContainer[m_pCubeContainer.size() - 1]->getScale().x))
			||
			(m_pCubeContainer[i]->getPosition().x - (m_pCubeContainer[i]->getRadius() * m_pCubeContainer[i]->getScale().x)) < (m_pCubeContainer[m_pCubeContainer.size() - 1]->getPosition().x - (m_pCubeContainer[m_pCubeContainer.size() - 1]->getRadius() * m_pCubeContainer[m_pCubeContainer.size() - 1]->getScale().x)))
		{
			glm::vec3 tempVelocity = m_pCubeContainer[i]->getVelocity();
			tempVelocity.x *= -1;
			m_pCubeContainer[i]->setVelocity(tempVelocity);
		}

		if ((m_pCubeContainer[i]->getPosition().y + (m_pCubeContainer[i]->getRadius() * m_pCubeContainer[i]->getScale().y)) > (m_pCubeContainer[m_pCubeContainer.size() - 1]->getPosition().y + (m_pCubeContainer[m_pCubeContainer.size() - 1]->getRadius() * m_pCubeContainer[m_pCubeContainer.size() - 1]->getScale().y))
			||
			(m_pCubeContainer[i]->getPosition().y - (m_pCubeContainer[i]->getRadius() * m_pCubeContainer[i]->getScale().y)) < (m_pCubeContainer[m_pCubeContainer.size() - 1]->getPosition().y - (m_pCubeContainer[m_pCubeContainer.size() - 1]->getRadius() * m_pCubeContainer[m_pCubeContainer.size() - 1]->getScale().y)))
		{
			glm::vec3 tempVelocity = m_pCubeContainer[i]->getVelocity();
			tempVelocity.y *= -1;
			m_pCubeContainer[i]->setVelocity(tempVelocity);
		}

		if ((m_pCubeContainer[i]->getPosition().z + (m_pCubeContainer[i]->getRadius() * m_pCubeContainer[i]->getScale().z)) > (m_pCubeContainer[m_pCubeContainer.size() - 1]->getPosition().z + (m_pCubeContainer[m_pCubeContainer.size() - 1]->getRadius() * m_pCubeContainer[m_pCubeContainer.size() - 1]->getScale().z))
			||
			(m_pCubeContainer[i]->getPosition().z - (m_pCubeContainer[i]->getRadius() * m_pCubeContainer[i]->getScale().z)) < (m_pCubeContainer[m_pCubeContainer.size() - 1]->getPosition().z - (m_pCubeContainer[m_pCubeContainer.size() - 1]->getRadius() * m_pCubeContainer[m_pCubeContainer.size() - 1]->getScale().z)))
		{
			glm::vec3 tempVelocity = m_pCubeContainer[i]->getVelocity();
			tempVelocity.z *= -1;
			m_pCubeContainer[i]->setVelocity(tempVelocity);
		}
	}
}

void Game::getCubePoints(Cube* cube1, Cube* cube2)
{
	P1[0].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * -1);
	P1[0].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * -1);
	P1[0].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * +1);
	glm::rotateX(P1[0], cube1->getRotation().x);
	glm::rotateY(P1[0], cube1->getRotation().y);
	glm::rotateZ(P1[0], cube1->getRotation().z);

	P1[1].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * +1);
	P1[1].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * -1);
	P1[1].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * +1);
	glm::rotateX(P1[1], cube1->getRotation().x);
	glm::rotateY(P1[1], cube1->getRotation().y);
	glm::rotateZ(P1[1], cube1->getRotation().z);

	P1[2].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * +1);
	P1[2].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * +1);
	P1[2].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * +1);
	glm::rotateX(P1[2], cube1->getRotation().x);
	glm::rotateY(P1[2], cube1->getRotation().y);
	glm::rotateZ(P1[2], cube1->getRotation().z);

	P1[3].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * -1);
	P1[3].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * +1);
	P1[3].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * +1);
	glm::rotateX(P1[3], cube1->getRotation().x);
	glm::rotateY(P1[3], cube1->getRotation().y);
	glm::rotateZ(P1[3], cube1->getRotation().z);

	P1[4].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * -1);
	P1[4].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * -1);
	P1[4].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * -1);
	glm::rotateX(P1[4], cube1->getRotation().x);
	glm::rotateY(P1[4], cube1->getRotation().y);
	glm::rotateZ(P1[4], cube1->getRotation().z);

	P1[5].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * +1);
	P1[5].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * -1);
	P1[5].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * -1);
	glm::rotateX(P1[5], cube1->getRotation().x);
	glm::rotateY(P1[5], cube1->getRotation().y);
	glm::rotateZ(P1[5], cube1->getRotation().z);

	P1[6].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * +1);
	P1[6].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * +1);
	P1[6].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * -1);
	glm::rotateX(P1[6], cube1->getRotation().x);
	glm::rotateY(P1[6], cube1->getRotation().y);
	glm::rotateZ(P1[6], cube1->getRotation().z);

	P1[7].x = cube1->getPosition().x + (cube1->getRadius() * cube1->getScale().x * -1);
	P1[7].y = cube1->getPosition().y + (cube1->getRadius() * cube1->getScale().y * +1);
	P1[7].z = cube1->getPosition().z + (cube1->getRadius() * cube1->getScale().z * -1);
	glm::rotateX(P1[7], cube1->getRotation().x);
	glm::rotateY(P1[7], cube1->getRotation().y);
	glm::rotateZ(P1[7], cube1->getRotation().z);


	P2[0].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * -1);
	P2[0].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * -1);
	P2[0].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * +1);
	glm::rotateX(P2[0], cube2->getRotation().x);
	glm::rotateY(P2[0], cube2->getRotation().y);
	glm::rotateZ(P2[0], cube2->getRotation().z);

	P2[1].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * +1);
	P2[1].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * -1);
	P2[1].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * +1);
	glm::rotateX(P2[1], cube2->getRotation().x);
	glm::rotateY(P2[1], cube2->getRotation().y);
	glm::rotateZ(P2[1], cube2->getRotation().z);

	P2[2].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * +1);
	P2[2].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * +1);
	P2[2].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * +1);
	glm::rotateX(P2[2], cube2->getRotation().x);
	glm::rotateY(P2[2], cube2->getRotation().y);
	glm::rotateZ(P2[2], cube2->getRotation().z);

	P2[3].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * -1);
	P2[3].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * +1);
	P2[3].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * +1);
	glm::rotateX(P2[3], cube2->getRotation().x);
	glm::rotateY(P2[3], cube2->getRotation().y);
	glm::rotateZ(P2[3], cube2->getRotation().z);

	P2[4].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * -1);
	P2[4].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * -1);
	P2[4].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * -1);
	glm::rotateX(P2[4], cube2->getRotation().x);
	glm::rotateY(P2[4], cube2->getRotation().y);
	glm::rotateZ(P2[4], cube2->getRotation().z);

	P2[5].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * +1);
	P2[5].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * -1);
	P2[5].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * -1);
	glm::rotateX(P2[5], cube2->getRotation().x);
	glm::rotateY(P2[5], cube2->getRotation().y);
	glm::rotateZ(P2[5], cube2->getRotation().z);

	P2[6].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * +1);
	P2[6].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * +1);
	P2[6].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * -1);
	glm::rotateX(P2[6], cube2->getRotation().x);
	glm::rotateY(P2[6], cube2->getRotation().y);
	glm::rotateZ(P2[6], cube2->getRotation().z);

	P2[7].x = cube2->getPosition().x + (cube2->getRadius() * cube2->getScale().x * -1);
	P2[7].y = cube2->getPosition().y + (cube2->getRadius() * cube2->getScale().y * +1);
	P2[7].z = cube2->getPosition().z + (cube2->getRadius() * cube2->getScale().z * -1);
	glm::rotateX(P2[7], cube2->getRotation().x);
	glm::rotateY(P2[7], cube2->getRotation().y);
	glm::rotateZ(P2[7], cube2->getRotation().z);
}

int Game::closestPointIndex(Cube* cube, glm::vec3* cubePoints)
{
	glm::vec3 cubePosition = cube->getPosition();
	float smallestDistance = 9999.f;
	float smallestDistance2 = 9999.f;
	
	int smallestIndex;
	// second smallest index for now global

	for (int i = 0; i < 8; i++)
	{
		if (i == 0)
		{
			smallestIndex = secondSmallestIndex = i;
			smallestDistance = smallestDistance2 = glm::distance(cubePosition, cubePoints[i]);
		}
		else
		{
			if (smallestDistance > glm::distance(cubePosition, cubePoints[i]))
			{
				secondSmallestIndex = smallestIndex;
				smallestDistance2 = smallestDistance;
				smallestIndex = i;
				smallestDistance = glm::distance(cubePosition, cubePoints[i]);
			}
			else if (smallestDistance2 > glm::distance(cubePosition, cubePoints[i]))
			{
				secondSmallestIndex = i;
				smallestDistance2 = glm::distance(cubePosition, cubePoints[i]);
			}
		}
	}

	return smallestIndex;

	
}

void Game::resolveCollision(Cube* cube1, Cube* cube2, glm::vec3 poc)
{
	// Normal
	glm::vec3 n = cube1->getPosition() - cube2->getPosition();

	// normalize
	n = glm::normalize(m_pSat->getMTV());

	// radius from poc
	glm::vec3 cube1Radius = poc - cube1->getPosition();
	glm::vec3 cube2Radius = poc - cube2->getPosition();

	// vel at poc
	glm::vec3 cube1Vel = cube1->getOldVelocity() + glm::cross(cube1->getOldAngularVelocity(), cube1Radius);
	glm::vec3 cube2Vel = cube2->getOldVelocity() + glm::cross(cube2->getOldAngularVelocity(), cube2Radius);

	// relative vel cube1 wrt cube2
	glm::vec3 relVel12 = cube1Vel - cube2Vel;

	// moment of inertia at poc
	// MI = 1/12 * M * width^2 * height^2
	float inertiaCube1 = cube1->getMass() * ((cube1Radius.x*cube1Radius.x) + (cube1Radius.y*cube1Radius.y) + (cube1Radius.z*cube1Radius.z));
	float inertiaCube2 = cube2->getMass() * ((cube2Radius.x*cube2Radius.x) + (cube2Radius.y*cube2Radius.y) + (cube2Radius.z*cube2Radius.z));
	
	// impulse
	float j;

	// numerator
	float num = (1 + 1 /* coefficient of restitution: 1 in this case */ * glm::dot(relVel12, n));

	// denominator
	float den = glm::dot((((1.f / cube1->getMass()) + (1.f / cube2->getMass()))*n) + ((glm::cross(glm::cross(cube1Radius, n), cube1Radius)) / inertiaCube1) + ((glm::cross(glm::cross(cube2Radius, n), cube2Radius)) / inertiaCube2), n);

	j = num / den;

	// final velocity 
	glm::vec3 cube1FVel = cube1->getOldVelocity() - ((n*j) / cube1->getMass());
	glm::vec3 cube2FVel = cube2->getOldVelocity() - ((n*j) / cube2->getMass());

	cube1->setVelocity(cube1FVel);
	cube2->setVelocity(-cube2FVel);

	// final angular velocity
	glm::vec3 cube1FAVel = cube1->getOldAngularVelocity() - (glm::cross(cube1Radius, n*j) / inertiaCube1);
	glm::vec3 cube2FAVel = cube2->getOldAngularVelocity() + (glm::cross(cube2Radius, n*j) / inertiaCube2);

	cube1->setAngularVelocity(cube1FAVel);
	cube2->setAngularVelocity(-cube2FAVel);
}