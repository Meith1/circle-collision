#ifndef __Circle__
#define __Circle__

#include "Mesh.h"
#include "GLSLShader.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

const int SLICE = 100;
const float STEP = 2 * glm::pi <float>() / SLICE;

class Circle
{
public:
	Circle();
	~Circle();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

private:

	Mesh m_pCircleMesh;
	Vertex m_pVertices[SLICE + 1];
	int m_pNumVertices;
	GLushort m_pIndices[3 * SLICE];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	float m_pRadius;
	glm::vec3 m_pVelocity;
	glm::vec3 m_pRotation;
};

#endif