#ifndef __MVP__
#define __MVP__

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class MVP
{
public:
	MVP();
	~MVP();

private:
	glm::mat4 m_pMVP;
};

#endif
