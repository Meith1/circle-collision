#include "Circle.h"

#include <cmath>

Circle::Circle()
{
	
}

Circle::~Circle()
{
}

void Circle::init(GLSLShader* shader)
{
	//initialise all variables
	m_pRadius = 0.5f;
	m_pNumVertices = SLICE + 1;
	m_pNumIndices = 3 * SLICE;
	m_pPolygonMode = GL_FILL;
	m_pRendererMode = GL_TRIANGLE_FAN;

	float angle = 0;
	m_pVertices[0].position.x = m_pVertices[0].position.y = m_pVertices[0].position.z = 0;
	for (int i = 1; i <= SLICE; i++, angle = angle + STEP)
	{
		m_pVertices[i].position.x = m_pRadius*cos(angle);
		m_pVertices[i].position.y = m_pRadius*sin(angle);
		m_pVertices[i].position.z = 0;
	}

	for (int i = 0; i <= SLICE; i++)
	{
		m_pVertices[i].color.r = 0;
		m_pVertices[i].color.g = 0;
		m_pVertices[i].color.b = 0;
	}

	int prev = 1;

	for (int i = 0; i < 3 * SLICE; i += 3)
	{
		m_pIndices[i] = 0;
		m_pIndices[i + 1] = prev;
		m_pIndices[i + 2] = prev + 1;
		prev++;
	}

	m_pIndices[(3 * SLICE) - 1] = 1;

	m_pVelocity = glm::vec3(0.1f, 0.1f, 0.1f);
	m_pRotation = glm::vec3(0.1f, 0.1f, 0.1f);

	//call mesh init
	//m_pCircleMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader, );
}

void Circle::update()
{
//	m_pCircleMesh.update(m_pVelocity, m_pRotation);
}

void Circle::render(GLSLShader* shader)
{
	m_pCircleMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader);
}

void Circle::close()
{
	m_pCircleMesh.close();
}