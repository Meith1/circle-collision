#ifndef __SAT__
#define __SAT__

#include "Cube.h"

struct BoundingBox
{
	glm::vec4 m_pPc; // coordinate position of center of cube 
	glm::vec4 m_pCx; // unit vector representing the x-axis of cube 
	glm::vec4 m_pCy; // unit vector representing the y-axis of cube 
	glm::vec4 m_pCz; // unit vector representing the z-axis of cube 
	float m_pWc; // half width of cube 
	float m_pHc; // half height of cube 
	float m_pDc; // half depth of cube 
};

class SAT
{
public:
	SAT();
	SAT(Cube* cubeA, Cube* cubeB);
	~SAT();

	void init();
	bool update();
	bool computeIntersection(glm::vec3 T, BoundingBox* A, BoundingBox* B);
	void setWhoCameLast(int index);

	glm::vec3 getMTV() { return m_pMTV; }
	int getCollisionType() { return collisionType; }

private:

	BoundingBox m_pBoxA;
	BoundingBox m_pBoxB;
	
	glm::vec3 m_pT; // T = m_pPa - m_pPb

	glm::vec4 m_pMTVAxis;
	float m_pMTVMag;
	glm::vec3 m_pMTV;

	Cube* m_pCubeA;
	Cube* m_pCubeB;

	bool iCameLast[15];
	int count;
	int collisionType;
};

#endif
