#ifndef __Mesh__
#define __Mesh__

#include "Vertex.h"
#include "GLSLShader.h"
#include "Camera.h"

#include <GL/glew.h>
#include <glm/glm.hpp>

class Mesh
{
public:
	Mesh();
	~Mesh();

	void init(Vertex vertices[], int numVertices, GLushort indices[], int numIndices, GLSLShader* shader, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);
	void update(glm::vec3 position, glm::vec3 rotation);
	void render(GLenum polygonMode, GLenum renderModem, int numIndices, GLSLShader* shader);
	void close();

	glm::mat4 getModelMatrix() { return m_pModelMatrix; }
	glm::mat4 getRotationMatrix() { return m_pRotationMatrix; }

private:
	GLuint m_pVaoID;
	GLuint m_pVboID;
	GLuint m_pVioID;

	glm::vec3 m_pTranslationVector;
	glm::vec3 m_pRotationVector;
	glm::vec3 m_pScaleVector;

	glm::mat4 m_pRotationMatrix;
	glm::mat4 m_pTranslationMatrix;
	glm::mat4 m_pScalingMatrix;
	glm::mat4 m_pModelMatrix;

	glm::mat4 m_pMVP;
};

#endif
