#ifndef __Window__
#define __Window__

#include <SDL/SDL.h>
#include <string>

class Window
{
public:
	static Window* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Window();
			return s_pInstance;
		}

		return s_pInstance;
	}

	int createWindow(std::string windowName, int screenWidth, int screenHeight, int currentFlags);
	SDL_Window* getwindow() { return m_pWindow; }
	void swapBuffers();

private:

	Window();
	~Window();

	static Window* s_pInstance;
	SDL_Window* m_pWindow;
	int m_pScreenWidth, m_pScreenHeight;
};

#endif
