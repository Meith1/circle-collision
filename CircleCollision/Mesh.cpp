#include "Mesh.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <iostream>

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::init(Vertex vertices[], int numVertices, GLushort indices[], int numIndices, GLSLShader* shader, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
{
	//setup mesh vao and vbo stuff
	glGenVertexArrays(1, &m_pVaoID);
	glGenBuffers(1, &m_pVboID);
	glGenBuffers(1, &m_pVioID);
	GLsizei stride = sizeof(Vertex);

	//bind vao and vbo stuff
	glBindVertexArray(m_pVaoID);

	glBindBuffer(GL_ARRAY_BUFFER, m_pVboID);

	//pass mesh vertices to buffer objects
	glBufferData(GL_ARRAY_BUFFER, stride*numVertices, &vertices[0], GL_STATIC_DRAW);

	//enable vertex attribute array for position
	glEnableVertexAttribArray((*shader)["vVertex"]);
	glVertexAttribPointer((*shader)["vVertex"], 3, GL_FLOAT, GL_FALSE, stride, 0);

	//enable vertex attribute for color
	glEnableVertexAttribArray((*shader)["vColor"]);
	glVertexAttribPointer((*shader)["vColor"], 3, GL_FLOAT, GL_FALSE, stride, (const GLvoid*)offsetof(Vertex, color));

	//pass indices to element array buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pVioID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);

	//glBindVertexArray(0);

	m_pTranslationVector = position;
	m_pRotationVector = rotation;
	m_pScaleVector = scale;

	std::cout << "Mesh init successful.\n";
}

void Mesh::update(glm::vec3 position, glm::vec3 rotation)
{
	m_pTranslationVector = position;
//	m_pTranslationVector += glm::vec3(velocity);
	m_pRotationVector = glm::vec3(rotation);
	//m_pRotationVector += glm::vec3(angularVelocity);
	m_pTranslationMatrix = glm::translate(glm::mat4(), m_pTranslationVector);
	m_pRotationMatrix = glm::eulerAngleYXZ(m_pRotationVector.y, m_pRotationVector.x, m_pRotationVector.z);
	m_pScalingMatrix = glm::scale(glm::mat4(), m_pScaleVector);

	m_pModelMatrix = m_pTranslationMatrix * m_pRotationMatrix * m_pScalingMatrix;

//	m_pMVP = m_pProjection.getProjectionMatrix() * m_pCamera.getViewMatrix() * m_pModelMatrix;
	m_pMVP = Camera::Instance()->getProjectionMatrix() * Camera::Instance()->getViewMatrix() * m_pModelMatrix;
}

void Mesh::render(GLenum polygonMode, GLenum renderMode, int numIndices, GLSLShader* shader)
{
	shader->use();
	{
		glBindVertexArray(m_pVaoID);
		glUniformMatrix4fv((*shader)("MVP"), 1, GL_FALSE, &m_pMVP[0][0]);
		{
			//set polygon mode
			glPolygonMode(GL_FRONT_AND_BACK, polygonMode);
			//draw mesh
			glDrawElements(renderMode, numIndices, GL_UNSIGNED_SHORT, 0);
		}
	}
	shader->unUse();
}

void Mesh::close()
{
	//Destroy vao and vbo
	glDeleteBuffers(1, &m_pVboID);
	glDeleteBuffers(1, &m_pVioID);
	glDeleteVertexArrays(1, &m_pVaoID);

	cout << "mesh close successful\n";
}