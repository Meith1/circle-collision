#include "SAT.h"

#include <iostream>


SAT::SAT()
{
}

SAT::SAT(Cube* cubeA, Cube* cubeB)
{
	m_pCubeA = cubeA;
	m_pCubeB = cubeB;

	m_pMTVMag = 1000000.f;
	m_pMTVAxis = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	for (int i = 0; i < 15; i++)
		iCameLast[i] = false;

	count = 0;
}

SAT::~SAT()
{
}

void SAT::setWhoCameLast(int index)
{
	for (int i = 0; i < 15; i++)
		iCameLast[i] = false;
	iCameLast[index] = true;
}

void SAT::init()
{
	m_pBoxA.m_pPc = glm::vec4(m_pCubeA->getPosition(), 0.0f);
	m_pBoxA.m_pCx = m_pCubeA->getRotationMatrix() * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f);
	m_pBoxA.m_pCy = m_pCubeA->getRotationMatrix() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
	m_pBoxA.m_pCz = m_pCubeA->getRotationMatrix() * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);
	m_pBoxA.m_pWc = (m_pCubeA->getRadius() * m_pCubeA->getScale().x);
	m_pBoxA.m_pHc = (m_pCubeA->getRadius() * m_pCubeA->getScale().y);
	m_pBoxA.m_pDc = (m_pCubeA->getRadius() * m_pCubeA->getScale().z);

	m_pBoxB.m_pPc = glm::vec4(m_pCubeB->getPosition(), 0.0f);
	m_pBoxB.m_pCx = m_pCubeB->getRotationMatrix() * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f);
	m_pBoxB.m_pCy = m_pCubeB->getRotationMatrix() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
	m_pBoxB.m_pCz = m_pCubeB->getRotationMatrix() * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);
	m_pBoxB.m_pWc = (m_pCubeB->getRadius() * m_pCubeB->getScale().x);
	m_pBoxB.m_pHc = (m_pCubeB->getRadius() * m_pCubeB->getScale().y);
	m_pBoxB.m_pDc = (m_pCubeB->getRadius() * m_pCubeB->getScale().z);
}

bool SAT::update()
{
	m_pT = glm::vec3(m_pBoxA.m_pPc - m_pBoxB.m_pPc);
	bool checkSA = computeIntersection(m_pT, &m_pBoxA, &m_pBoxB);
	if (!checkSA)
	{
		/*std::cout << "Collided\n";
		std::cout << "intersection in axis: " << m_pMTVAxis.x << m_pMTVAxis.y << m_pMTVAxis.z << " Magnitude: " << m_pMTVMag << "\n";
		m_pCubeA->setVelocity(glm::vec3(0));
		m_pCubeB->setVelocity(glm::vec3(0));
		m_pCubeA->setAngularVelocity(glm::vec3(0));
		m_pCubeB->setAngularVelocity(glm::vec3(0));*/

		return true;
	}
	/*else
	{
		std::cout << " not Collided\n";
	}*/
	return false;
}

bool SAT::computeIntersection(glm::vec3 T, BoundingBox* A, BoundingBox* B)
{
	// box A x-axis
	glm::vec4 newT = glm::vec4(T, 0);
	if (abs(glm::dot(newT, A->m_pCx)) > (A->m_pWc 
										+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCx))
										+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCy))
										+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCz))))
	{
		//std::cout << "no intersection in a-x axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > (A->m_pWc
			+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCx))
			+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCy))
			+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCz))))
		{
			m_pMTVMag = (A->m_pWc
				+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCx))
				+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCy))
				+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCz)));

			m_pMTVAxis = A->m_pCx;
			collisionType = 1;
		}
	}

	// box A y-axis
	if (abs(glm::dot(newT, A->m_pCy)) > (A->m_pHc
										+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCx))
										+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCy))
										+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCz))))
	{
		//std::cout << "no intersection in a-y axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > (A->m_pHc
			+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCx))
			+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCy))
			+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCz))))
		{
			m_pMTVMag = (A->m_pHc
				+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCx))
				+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCy))
				+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCz)));

			m_pMTVAxis = A->m_pCy;
			collisionType = 1;
		}
	}

	// box A z-axis
	if (abs(glm::dot(newT, A->m_pCz)) > (A->m_pDc
										+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCx))
										+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCy))
										+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCz))))
	{
		//std::cout << "no intersection in a-z axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > (A->m_pDc
			+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCx))
			+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCy))
			+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCz))))
		{
			m_pMTVMag = (A->m_pDc
				+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCx))
				+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCy))
				+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCz)));

			m_pMTVAxis = A->m_pCz;
			collisionType = 1;
		}
	}

	// Box B x-axis
	if (abs(glm::dot(newT, B->m_pCx)) > (B->m_pWc
										+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCx))
										+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCx))
										+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCx))))
	{
		//std::cout << "no intersection in b-x axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >  (B->m_pWc
			+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCx))
			+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCx))
			+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCx))))
		{
			m_pMTVMag = (B->m_pWc
				+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCx))
				+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCx))
				+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCx)));

			m_pMTVAxis = B->m_pCx;
			collisionType = 1;
		}
	}

	// Box B y-axis
	if (abs(glm::dot(newT, B->m_pCy)) > (B->m_pHc
										+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCy))
										+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCy))
										+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCy))))
	{
		//std::cout << "no intersection in b-y axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >  (B->m_pHc
			+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCy))
			+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCy))
			+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCy))))
		{
			m_pMTVMag = (B->m_pHc
				+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCy))
				+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCy))
				+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCy)));

			m_pMTVAxis = B->m_pCy;
			collisionType = 1;       
		}
	}

	// Box B z-axis
	if (abs(glm::dot(newT, B->m_pCz)) > (B->m_pDc
										+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCz))
										+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCz))
										+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCz))))
	{
		//std::cout << "no intersection in b-x axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >  (B->m_pDc
			+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCz))
			+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCz))
			+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCz))))
		{
			m_pMTVMag = (B->m_pDc
				+ abs(A->m_pWc * glm::dot(A->m_pCx, B->m_pCz))
				+ abs(A->m_pHc * glm::dot(A->m_pCy, B->m_pCz))
				+ abs(A->m_pDc * glm::dot(A->m_pCz, B->m_pCz)));

			m_pMTVAxis = B->m_pCz;
			collisionType = 1;
		}
	}

	// Ax-Bx
	if (abs(glm::dot(newT, A->m_pCz)*glm::dot(A->m_pCy, B->m_pCx)
		- glm::dot(newT, A->m_pCy)*glm::dot(A->m_pCz, B->m_pCx)) >
																	abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCx))
																	+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCx))
																	+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCz))
																	+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCy)))
	{
		//std::cout << "no intersection in ax-bx axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >  abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCx))
			+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCx))
			+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCz))
			+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCy)))
		{
			m_pMTVMag = abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCx))
				+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCx))
				+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCz))
				+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCy));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCx), glm::vec3(B->m_pCx)),0.f);
			collisionType = 2;
		}
	}

	// Ax-By
	if (abs(glm::dot(newT, A->m_pCz)*glm::dot(A->m_pCy, B->m_pCy)
		- glm::dot(newT, A->m_pCy)*glm::dot(A->m_pCz, B->m_pCy)) >
																	abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCy))
																	+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCy))
																	+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCz))
																	+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCx)))
	{
		//std::cout << "no intersection in ax-by axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCy))
			+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCy))
			+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCz))
			+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCx)))
		{
			m_pMTVMag = abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCy))
				+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCy))
				+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCz))
				+ abs(B->m_pDc * glm::dot(A->m_pCx, B->m_pCx));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCx), glm::vec3(B->m_pCy)), 0.f);
			collisionType = 2;
		}
	}

	// Ax-Bz
	if (abs(glm::dot(newT, A->m_pCz)*glm::dot(A->m_pCy, B->m_pCz)
		- glm::dot(newT, A->m_pCy)*glm::dot(A->m_pCz, B->m_pCz)) >
																	abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCz))
																	+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCz))
																	+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCy))
																	+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCx)))
	{
		//std::cout << "no intersection in ax-bz axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCz))
			+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCz))
			+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCy))
			+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCx)))
		{
			m_pMTVMag = abs(A->m_pHc * glm::dot(A->m_pCz, B->m_pCz))
				+ abs(A->m_pDc * glm::dot(A->m_pCy, B->m_pCz))
				+ abs(B->m_pWc * glm::dot(A->m_pCx, B->m_pCy))
				+ abs(B->m_pHc * glm::dot(A->m_pCx, B->m_pCx));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCx), glm::vec3(B->m_pCz)), 0.f);
			collisionType = 2;
		}
	}

	// Ay-Bx
	if (abs(glm::dot(newT, A->m_pCx)*glm::dot(A->m_pCz, B->m_pCx)
		- glm::dot(newT, A->m_pCz)*glm::dot(A->m_pCx, B->m_pCx)) >
																	abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCx))
																	+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCx))
																	+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCz))
																	+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCy)))
	{
		//std::cout << "no intersection in ay-bx axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCx))
			+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCx))
			+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCz))
			+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCy)))
		{
			m_pMTVMag = abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCx))
				+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCx))
				+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCz))
				+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCy));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCy), glm::vec3(B->m_pCx)), 0.f);
			collisionType = 2;
		}
	}

	// Ay-By
	if (abs(glm::dot(newT, A->m_pCx)*glm::dot(A->m_pCz, B->m_pCy)
		- glm::dot(newT, A->m_pCz)*glm::dot(A->m_pCx, B->m_pCy)) >
																	abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCy))
																	+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCy))
																	+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCz))
																	+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCx)))
	{
		//std::cout << "no intersection in ay-by axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag > abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCy))
			+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCy))
			+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCz))
			+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCx)))
		{
			m_pMTVMag = abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCy))
				+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCy))
				+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCz))
				+ abs(B->m_pDc * glm::dot(A->m_pCy, B->m_pCx));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCy), glm::vec3(B->m_pCy)), 0.f);
			collisionType = 2;
		}
	}

	// Ay-Bz
	if (abs(glm::dot(newT, A->m_pCx)*glm::dot(A->m_pCz, B->m_pCz)
		- glm::dot(newT, A->m_pCz)*glm::dot(A->m_pCx, B->m_pCz)) >
																	abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCz))
																	+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCz))
																	+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCy))
																	+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCx)))
	{
		//std::cout << "no intersection in ay-bz axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCz))
			+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCz))
			+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCy))
			+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCx)))
		{
			m_pMTVMag = abs(A->m_pWc * glm::dot(A->m_pCz, B->m_pCz))
				+ abs(A->m_pDc * glm::dot(A->m_pCx, B->m_pCz))
				+ abs(B->m_pWc * glm::dot(A->m_pCy, B->m_pCy))
				+ abs(B->m_pHc * glm::dot(A->m_pCy, B->m_pCx));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCy), glm::vec3(B->m_pCz)), 0.f);
			collisionType = 2;
		}
	}

	// Az-Bx
	if (abs(glm::dot(newT, A->m_pCy)*glm::dot(A->m_pCx, B->m_pCx)
		- glm::dot(newT, A->m_pCx)*glm::dot(A->m_pCy, B->m_pCx)) >
																	abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCx))
																	+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCx))
																	+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCz))
																	+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCy)))
	{
		//std::cout << "no intersection in az-bx axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCx))
			+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCx))
			+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCz))
			+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCy)))
		{
			m_pMTVMag = abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCx))
				+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCx))
				+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCz))
				+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCy));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCz), glm::vec3(B->m_pCx)), 0.f);
			collisionType = 2;
		}
	}

	// Az-By
	if (abs(glm::dot(newT, A->m_pCy)*glm::dot(A->m_pCx, B->m_pCy)
		- glm::dot(newT, A->m_pCx)*glm::dot(A->m_pCy, B->m_pCy)) >
																	abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCy))
																	+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCy))
																	+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCz))
																	+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCx)))
	{
		//std::cout << "no intersection in az-by axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCy))
			+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCy))
			+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCz))
			+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCx)))
		{
			m_pMTVMag = abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCy))
				+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCy))
				+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCz))
				+ abs(B->m_pDc * glm::dot(A->m_pCz, B->m_pCx));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCz), glm::vec3(B->m_pCy)), 0.f);
			collisionType = 2;
		}
	}

	// Az-Bz
	if (abs(glm::dot(newT, A->m_pCy)*glm::dot(A->m_pCx, B->m_pCz)
		- glm::dot(newT, A->m_pCx)*glm::dot(A->m_pCy, B->m_pCz)) >
																	abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCz))
																	+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCz))
																	+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCy))
																	+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCx)))
	{
		//std::cout << "no intersection in az-bz axis\n";
		return true;
	}
	else
	{
		if (m_pMTVMag >abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCz))
			+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCz))
			+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCy))
			+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCx)))
		{
			m_pMTVMag = abs(A->m_pWc * glm::dot(A->m_pCy, B->m_pCz))
				+ abs(A->m_pHc * glm::dot(A->m_pCx, B->m_pCz))
				+ abs(B->m_pWc * glm::dot(A->m_pCz, B->m_pCy))
				+ abs(B->m_pHc * glm::dot(A->m_pCz, B->m_pCx));

			m_pMTVAxis = glm::vec4(glm::cross(glm::vec3(A->m_pCz), glm::vec3(B->m_pCz)), 0.f);
			collisionType = 2;
		}
	}

	m_pMTV = glm::vec3(m_pMTVAxis)*m_pMTVMag;
	return false;
}
