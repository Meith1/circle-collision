#include "Window.h"

#include<GL/glew.h>

#include <iostream>

Window* Window::s_pInstance = 0;

Window::Window()
{
}

Window::~Window()
{
}

int Window::createWindow(std::string windowName, int screenWidth, int screenHeight, int currentFlags)
{
	Uint32 flags = SDL_WINDOW_OPENGL;
	m_pWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
	if (m_pWindow == NULL)
	{
		std::cout << "SDL window could not be created\n";
	}

	SDL_GLContext glContext = SDL_GL_CreateContext(m_pWindow);
	if (glContext == NULL)
	{
		std::cout << "SDL_GL context could not be created\n";
	}

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		std::cout << "Could not initialize glew\n";
	}

	std::printf("OpenGL version: %s\n", glGetString(GL_VERSION));

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	SDL_GL_SetSwapInterval(0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

void Window::swapBuffers()
{
	SDL_GL_SwapWindow(m_pWindow);
}