#include "GLSLShader.h"

#include <iostream>

GLSLShader::GLSLShader()
{
	m_pTotalShaders = 0;
	m_pShaders[VERTEX_SHADER] = 0;
	m_pShaders[FRAGMENT_SHADER] = 0;
	m_pShaders[GEOMETRY_SHADER] = 0;
	m_pAttributeList.clear();
	m_pUniformLocationList.clear();
}

GLSLShader::~GLSLShader()
{
	m_pAttributeList.clear();
	m_pUniformLocationList.clear();
}

void GLSLShader::deleteShaderProgram()
{
	glDeleteProgram(m_pProgram);
}

void GLSLShader::loadFromString(GLenum shaderType, const string& source)
{
	GLuint shader = glCreateShader(shaderType);

	const char* tempSource = source.c_str();
	glShaderSource(shader, 1, &tempSource, NULL);

	//check whether shader loads fine
	GLint status;
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* infoLog = new GLchar[infoLogLength];
		glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
		cerr << "Compile log: " << infoLog << endl;
		delete[] infoLog;
	}
	m_pShaders[m_pTotalShaders++] = shader;
}

void GLSLShader::createAndLinkProgram()
{
	m_pProgram = glCreateProgram();
	if (m_pShaders[VERTEX_SHADER] != 0)
	{
		glAttachShader(m_pProgram, m_pShaders[VERTEX_SHADER]);
	}
	if (m_pShaders[FRAGMENT_SHADER] != 0)
	{
		glAttachShader(m_pProgram, m_pShaders[FRAGMENT_SHADER]);
	}
	if (m_pShaders[GEOMETRY_SHADER] != 0)
	{
		glAttachShader(m_pProgram, m_pShaders[GEOMETRY_SHADER]);
	}

	//check whether shader program links fine
	GLint status;
	glLinkProgram(m_pProgram);
	glGetProgramiv(m_pProgram, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetShaderiv(m_pProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* infoLog = new GLchar[infoLogLength];
		glGetShaderInfoLog(m_pProgram, infoLogLength, NULL, infoLog);
		cerr << "Link log: " << infoLog << endl;
		delete[] infoLog;
	}

	glDeleteShader(m_pShaders[VERTEX_SHADER]);
	glDeleteShader(m_pShaders[FRAGMENT_SHADER]);
	glDeleteShader(m_pShaders[GEOMETRY_SHADER]);
}

void GLSLShader::use()
{
	glUseProgram(m_pProgram);
}

void GLSLShader::unUse()
{
	glUseProgram(0);
}

void GLSLShader::addAttribute(const string& attribute)
{
	m_pAttributeList[attribute] = glGetAttribLocation(m_pProgram, attribute.c_str());
}

//An indexer that returns the location of the attribute
GLuint GLSLShader::operator[](const string& attribute)
{
	return m_pAttributeList[attribute];
}

void GLSLShader::addUniform(const string& uniform)
{
	m_pUniformLocationList[uniform] = glGetUniformLocation(m_pProgram, uniform.c_str());
}

GLuint GLSLShader::operator()(const string& uniform)
{
	return m_pUniformLocationList[uniform];
}

#include <fstream>
void GLSLShader::loadFromFile(GLenum shaderType, const string& filename)
{
	ifstream shaderFile;
	shaderFile.open(filename.c_str(), ios_base::in);
	if (shaderFile)
	{
		string line, buffer;
		while (getline(shaderFile, line))
		{
			buffer.append(line);
			buffer.append("\r\n");
		}
		//copy to source
		loadFromString(shaderType, buffer);
	}
	else
	{
		cerr << "Error loading shader: " << filename << endl;
	}
}

