#include "Projection.h"

Projection::Projection()
{
	m_pProjectionMatrix = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);
}

Projection::~Projection()
{
}

glm::mat4 Projection::getProjectionMatrix()
{
	return m_pProjectionMatrix;
}