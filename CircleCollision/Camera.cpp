#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

Camera* Camera::s_pInstance = 0;

Camera::Camera()
{
	m_pCameraPosition = glm::vec3(0, 0, 50);
	m_pCameraDirection = glm::vec3(0, 0, 0);
	m_pCameraUpVector = glm::vec3(0, 1, 0);

	m_pProjectionMatrix = glm::perspective(
		45.0f, // FOV
		4.0f / 3.0f, // Aspect Ratio
		0.1f, // Near Plane
		100.0f // Far Plane
		);
}

Camera::~Camera()
{
}

glm::mat4 Camera::getViewMatrix()
{
	return m_pViewMatrix;
}

glm::mat4 Camera::getProjectionMatrix()
{
	return m_pProjectionMatrix;
}

void Camera::update()
{
	m_pViewMatrix = glm::lookAt(
		m_pCameraPosition, // Camera in World Space
		m_pCameraDirection, // and looking at origin (0,0,0)
		m_pCameraUpVector // head is up (positive y)
		);
}

void Camera::setCameraPosition(glm::vec3 position)
{
	m_pCameraPosition = position;
}

glm::vec3 Camera::getCameraPosition()
{
	return m_pCameraPosition;
}

void Camera::setCameraDirection(glm::vec3 direction)
{
	m_pCameraDirection = direction;
}

glm::vec3 Camera::getCameraDirection()
{
	return m_pCameraDirection;
}

glm::vec3 Camera::getCameraUpVector()
{
	return m_pCameraUpVector;
}

glm::vec3 Camera::CamLookDirection()
{
	return glm::normalize(m_pCameraDirection - m_pCameraPosition);
}