#include "Cube.h"

#include <cmath>

Cube::Cube()
{
}

Cube::~Cube()
{
}

void Cube::init(GLSLShader* shader)
{
	m_pRadius = 0.5f;
	m_pMass = pow(m_pRadius*m_pScale.x, 2);
	m_pNumVertices = 8;
	m_pNumIndices = 36;
	m_pPolygonMode = GL_LINE;
	m_pRendererMode = GL_TRIANGLES;

	//front face
	m_pVertices[0].position.x = -m_pRadius;
	m_pVertices[0].position.y = -m_pRadius;
	m_pVertices[0].position.z = +m_pRadius;

	m_pVertices[1].position.x = +m_pRadius;
	m_pVertices[1].position.y = -m_pRadius;
	m_pVertices[1].position.z = +m_pRadius;

	m_pVertices[2].position.x = +m_pRadius;
	m_pVertices[2].position.y = +m_pRadius;
	m_pVertices[2].position.z = +m_pRadius;

	m_pVertices[3].position.x = -m_pRadius;
	m_pVertices[3].position.y = +m_pRadius;
	m_pVertices[3].position.z = +m_pRadius;

	//back face
	m_pVertices[4].position.x = -m_pRadius;
	m_pVertices[4].position.y = -m_pRadius;
	m_pVertices[4].position.z = -m_pRadius;

	m_pVertices[5].position.x = +m_pRadius;
	m_pVertices[5].position.y = -m_pRadius;
	m_pVertices[5].position.z = -m_pRadius;

	m_pVertices[6].position.x = +m_pRadius;
	m_pVertices[6].position.y = +m_pRadius;
	m_pVertices[6].position.z = -m_pRadius;

	m_pVertices[7].position.x = -m_pRadius;
	m_pVertices[7].position.y = +m_pRadius;
	m_pVertices[7].position.z = -m_pRadius;

	//front face
	m_pIndices[0] = 0;
	m_pIndices[1] = 1;
	m_pIndices[2] = 2;
	m_pIndices[3] = 0;
	m_pIndices[4] = 2;
	m_pIndices[5] = 3;

	//back face
	m_pIndices[6] = 5;
	m_pIndices[7] = 4;
	m_pIndices[8] = 7;
	m_pIndices[9] = 5;
	m_pIndices[10] = 7;
	m_pIndices[11] = 6;

	//left face
	m_pIndices[12] = 4;
	m_pIndices[13] = 0;
	m_pIndices[14] = 3;
	m_pIndices[15] = 4;
	m_pIndices[16] = 3;
	m_pIndices[17] = 7;

	//right face
	m_pIndices[18] = 1;
	m_pIndices[19] = 5;
	m_pIndices[20] = 6;
	m_pIndices[21] = 1;
	m_pIndices[22] = 6;
	m_pIndices[23] = 2;

	//top face
	m_pIndices[24] = 3;
	m_pIndices[25] = 2;
	m_pIndices[26] = 6;
	m_pIndices[27] = 3;
	m_pIndices[28] = 6;
	m_pIndices[29] = 7;

	//bottom face
	m_pIndices[30] = 4;
	m_pIndices[31] = 5;
	m_pIndices[32] = 1;
	m_pIndices[33] = 4;
	m_pIndices[34] = 1;
	m_pIndices[35] = 0;

	m_pCubeMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader, m_pPosition, m_pRotation, m_pScale);
}

void Cube::update()
{
	m_pPosition += m_pVelocity;
	m_pRotation += m_pAngularVelocity;
	m_pCubeMesh.update(m_pPosition, m_pRotation);
}

void Cube::render(GLSLShader* shader)
{
	m_pCubeMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader);
}

void Cube::close()
{
	m_pCubeMesh.close();
}

void Cube::setPosition(glm::vec3 position)
{
	m_pPosition = position;
}

void Cube::setVelocity(glm::vec3 velocity)
{
	m_pVelocity = velocity;
}

void Cube::setOldVelocity(glm::vec3 oldVelocity)
{
	m_pOldVelocity = oldVelocity;
}

void Cube::setAngularVelocity(glm::vec3 angularVelocity)
{
	m_pAngularVelocity = angularVelocity;
}

void Cube::setOldAngularVelocity(glm::vec3 oldAngularVelocity)
{
	m_pOldAngularVelocity = oldAngularVelocity;
}

void Cube::setRotation(glm::vec3 rotation)
{
	m_pRotation = rotation;
}

void Cube::setScale(glm::vec3 scale)
{
	m_pScale = scale;
}

glm::vec3 Cube::getPosition()
{
	return m_pPosition;
}

glm::vec3 Cube::getVelocity()
{
	return m_pVelocity;
}

glm::vec3 Cube::getOldVelocity()
{
	return m_pOldVelocity;
}

glm::vec3 Cube::getAngularVelocity()
{
	return m_pAngularVelocity;
}

glm::vec3 Cube::getOldAngularVelocity()
{
	return m_pOldAngularVelocity;
}

glm::vec3 Cube::getRotation()
{
	return m_pRotation;
}

glm::vec3 Cube::getScale()
{
	return m_pScale;
}