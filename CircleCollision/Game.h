#ifndef __Game__
#define __Game__

#include "GLSLShader.h"
#include "Circle.h"
#include "Cube.h"
#include "SAT.h"

#include <vector>

class Game
{
public:

	static Game* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}

		return s_pInstance;
	}

	bool init();
	void handleEvents();
	void update();
	void render();
	void close();
	void quit();

	bool running() { return  m_bRunning; }

	void wallCollision();
	void getCubePoints(Cube* cube1, Cube* cube2);
	int closestPointIndex(Cube* cube, glm::vec3* cubePoints);
	void resolveCollision(Cube* cube1, Cube* cube2, glm::vec3 poc);

private:
	Game();
	~Game();

	static Game* s_pInstance;
	bool m_bRunning;
	bool m_bShapeInit;

	GLSLShader* m_pShader;
	//Circle m_pCircle;
	Cube* m_pCube;
	std::vector<Cube*> m_pCubeContainer;

	SAT* m_pSat;

	glm::vec3 P1[8];
	glm::vec3 P2[8];

	bool checkOrResolve;
};

#endif
