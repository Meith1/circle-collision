#ifndef __Cube__
#define __Cube__

#include "Mesh.h"
#include "GLSLShader.h"


#include <glm/glm.hpp>

class Cube
{
public:
	Cube();
	~Cube();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

	void setPosition(glm::vec3 positon);
	void setVelocity(glm::vec3 velocity);
	void setOldVelocity(glm::vec3 oldVelocity);
	void setAngularVelocity(glm::vec3 angularVelocity);
	void setOldAngularVelocity(glm::vec3 oldAngularVelocity);
	void setRotation(glm::vec3 rotation);
	void setScale(glm::vec3 scale);

	glm::vec3 getPosition();
	glm::vec3 getVelocity();
	glm::vec3 getOldVelocity();
	glm::vec3 getAngularVelocity();
	glm::vec3 getOldAngularVelocity();
	glm::vec3 getRotation();
	glm::vec3 getScale();
	float getRadius() { return m_pRadius; }
	float getMass() { return m_pMass; }

	glm::mat4 getModelMatrix() { return m_pCubeMesh.getModelMatrix(); }
	glm::mat4 getRotationMatrix() { return m_pCubeMesh.getRotationMatrix(); }

	void setVertexColor(glm::vec3 color) { for (int i = 0; i < 8; i++) m_pVertices[i].color = color; }
	Vertex* getVertices() { return m_pVertices; }

private:
	Mesh m_pCubeMesh;
	Vertex m_pVertices[8];
	int m_pNumVertices;
	GLushort m_pIndices[36];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	float m_pRadius;
	float m_pMass;
	glm::vec3 m_pPosition;
	glm::vec3 m_pRotation;
	glm::vec3 m_pScale;
	glm::vec3 m_pVelocity;
	glm::vec3 m_pOldVelocity;
	glm::vec3 m_pAngularVelocity;
	glm::vec3 m_pOldAngularVelocity;
};

#endif
